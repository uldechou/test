import { Routes } from '@angular/router';
import {AuthGuardService} from "./auth/core/auth.guard.service";

export const routes: Routes = [
  {path: 'auth', loadChildren: () => import('./auth/auth.routes').then(m => m.authRoutes)},
  {path: '', loadComponent: () => import('./home/home.component').then(m => m.HomeComponent), canActivate: [AuthGuardService],},
  {path: '**', loadComponent: () => import('./page-not-found/page-not-found.component').then(m => m.PageNotFoundComponent)},
];
