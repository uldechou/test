import {Routes} from "@angular/router";

export const authRoutes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadComponent: () => import('./login/login.component').then(m => m.LoginComponent)},
  {path: 'login', loadComponent: () => import('./login/login.component').then(m => m.LoginComponent)},
  {path: 'signup', loadComponent: () => import('./signup/signup.component').then(m => m.SignupComponent)},
  {path: 'forget-password', loadComponent: () => import('./forget-passord/forget-passord.component').then(m => m.ForgetPassordComponent)},
]
