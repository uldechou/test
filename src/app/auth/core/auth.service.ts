
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {catchError, throwError} from "rxjs";
import {UserLogin} from "../models/userLogin";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  private readonly storageToken = "dsfsdfggdfgfs57";

  login(user : UserLogin) {
    const formData: FormData = new FormData();
    let body = new URLSearchParams();
    body.set('username', user.username);
    body.set('password', user.password);
    body.set('client_id', 'frontend');
    body.set('grant_type', 'password');

    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    return this.http.post('https://auth.mind2codes.com/auth/realms/defis-devs/protocol/openid-connect/token', body.toString(), options)
      .pipe(catchError((err) => {
      return throwError(() => "error")
    }));
  }

  isLoggedIn() {
    return sessionStorage.getItem(this.storageToken) != "" && sessionStorage.getItem(this.storageToken) != undefined;
  }

}
