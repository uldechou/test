import {Component, OnInit} from '@angular/core';
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {UserLogin} from "../models/userLogin";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../core/auth.service";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    InputTextModule,
    ButtonModule,
    ReactiveFormsModule
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{

  userLoginForm : FormGroup;
  private readonly storageToken = "dsfsdfggdfgfs57";

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router) {
  }

  ngOnInit(): void {
        this.initForm();
  }

  initForm() {
    this.userLoginForm = this.fb.group({
      username: new FormControl(),
      password: new FormControl(),
    });
  }

  login() {

    this.authService.login(this.userLoginForm.value).subscribe(
      {
        next: (data: any) => {
          console.log('data => ', data);
          this.goodLogin(data.access_token);
        },
        error: () => {
        }
      }
    )

  }

  goodLogin(token: string) {

    sessionStorage.setItem(this.storageToken, token);
    this.router.navigate(['/']);

  }

}
